#pragma once

#include <EGL/egl.h>
#include <cstdint>

// Public functions are declared in trace_fixture.h.

// Private Functions

void SetupReplayContext1();
void ReplayContext1Frame1();
void ReplayContext1Frame2();
void ReplayContext1Frame3();
void ReplayContext1Frame4();
void ReplayContext1Frame5();
void ReplayContext1Frame6();
void ReplayContext1Frame7();
void ReplayContext1Frame8();
void ReplayContext1Frame9();
void ResetReplayContextShared();
void ResetReplayContext1();
void ReplayContext1Frame10();
void SetupReplayContextShared();
void InitReplay();

// Global variables

