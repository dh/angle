# ANGLE - Almost Native Graphics Layer Engine

This fork is specifically made for usage inside Mesa3D CI, with restricted feature-set for traces.

```bash
#!/usr/bin/env bash

set -e

# DEPOT tools
git clone --depth 1 https://chromium.googlesource.com/chromium/tools/depot_tools.git

PWD=$(pwd)
export PATH=$PWD/depot_tools:$PATH
export DEPOT_TOOLS_UPDATE=0

# ANGLE

ANGLE_REV=b7ee023e5a21e63d18f1a8c5fcd1ea4197f22c71

mkdir angle
cd angle
git init
git remote add origin https://gitlab.freedesktop.org/okias/angle.git
git fetch --depth 1 origin "$ANGLE_REV"
git checkout FETCH_HEAD

# preparation
python3 scripts/bootstrap.py
mkdir -p build/config
gclient sync

sed -i "/catapult/d" testing/BUILD.gn

mkdir -p out/Release
echo '
is_debug = false

angle_enable_swiftshader = false

angle_enable_null = false
angle_enable_gl = false
angle_has_histograms = false

build_angle_trace_perf_tests = true
angle_expose_non_conformant_extensions_and_versions = true
' > out/Release/args.gn

gn gen out/Release
autoninja -C out/Release/ angle_perftests

pushd out/Release/
rm args.gn ./*_deps ./*ninja* ./.ninja* ./*.TOC obj -rf
popd


# run
export vblank_mode=3
./out/Release/angle_perftests --gtest_output=json --one-frame-only --gtest_filter=TracePerfTest.Run/native_minetest --verbose-logging
```
